﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OracleUtility
{
    public class DatabaseConfig
    {
        public string host;
        public string port = "1521";
        public string servicename;
        public string username;
        public string password;

        public string checkScript = "";
        public string transferScript = "";
        public string tranferProcedure = "";

        public DatabaseConfig()
        {
        }

        public DatabaseConfig(string configPath)
        {
        }

        public string toString()
        {
            string rtn = "";

            rtn += "host:" + host;
            rtn += ";port:" + port;
            rtn += ";servicename:" + servicename;
            rtn += ";username:" + username;
            rtn += ";password:" + password;

            return rtn;
        }

        public void saveConfig()
        {
            try
            {
                string config = toString();
                string encryptStr = EncryptionManagement.Encrypt(config, true);

                string savePath = Directory.GetCurrentDirectory() + "\\db.config";
                if (File.Exists(savePath))
                {
                    File.Delete(savePath);
                }

                using (FileStream fs = File.Create(savePath))
                {
                    Byte[] savebytes = new UTF8Encoding(true).GetBytes(encryptStr);
                    fs.Write(savebytes, 0, savebytes.Length);
                    fs.Close();
                    fs.Dispose();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void loadConfig(string filePath)
        {
            try
            {
                using (StreamReader sr = File.OpenText(filePath))
                {
                    string encryptStr = sr.ReadToEnd();
                    string decryptStr = EncryptionManagement.Decrypt(encryptStr, true);

                    List<string> infos = decryptStr.Split(';').ToList();
                    foreach (var info in infos)
                    {
                        string[] data = info.Split(':');

                        string value = data.Length == 2 ? data[1] : "";
                        switch (data[0])
                        {
                            case "host": host = value; break;
                            case "port": port = value; break;
                            case "servicename": servicename = value; break;
                            case "username": username = value; break;
                            case "password": password = value; break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}