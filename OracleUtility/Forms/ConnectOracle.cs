﻿using OracleUtility.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace OracleUtility
{
    public partial class ConnectOracle : Form
    {
        private System.Timers.Timer t = new System.Timers.Timer();
        private int tCount = 0;
        public ConnectOracle()
        {
            InitializeComponent();
            t.Interval = 10;
        }

        private void openLoading()
        {

            Loading fLoading = new Loading();
            fLoading.ShowDialog();

            tCount = 0;
            t.Interval = 100;
            t.Enabled = true;
            t.Elapsed += new ElapsedEventHandler(timerLoading);
            t.Start();



        }

        private void timerLoading(object sender, ElapsedEventArgs e)
        {
            tCount++;
            Console.WriteLine(tCount);
            if (tCount >= 10)
            {
                t.Stop();
            }
        }

        private void btnCheckConnect_Click(object sender, EventArgs e)
        {
            openLoading();
        }
    }
}
