﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace OracleUtility
{
    public partial class Opening : Form
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        public Opening()
        {
            InitializeComponent();

            //this.BackColor = Color.LimeGreen;
            //this.TransparencyKey = Color.LimeGreen;

            this.Opacity = .01;
            timer.Interval = 25; //replace 10 with whatever you want
            timer.Tick += IncreaseOpacity;
            timer.Start();
        }

        void IncreaseOpacity(object sender, EventArgs e)
        {
            if (this.Opacity <= 1)  //replace 0.88 with whatever you want
            {
                this.Opacity += 0.01;  //replace 0.01 with whatever you want
            }
            if (this.Opacity == 1)
            {//replace 0.88 with whatever you want
                timer.Stop();
                Thread.Sleep(3000);
                this.Hide();

                ConnectOracle fConnect = new ConnectOracle();
                fConnect.FormClosed += (s, agrs) => this.Close();
                fConnect.Show();
            }
        }

    }


}
