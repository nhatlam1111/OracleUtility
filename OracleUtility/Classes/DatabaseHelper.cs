﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;

namespace OracleUtility
{
    public class DatabaseHelper
    {
        private OracleConnection _con;

        //private const string connectionString = "User Id={0};Password={1};Data Source=SERVER_247;";
        private const string connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1}))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME={2})));User ID={3};Password={4}";

        public DatabaseConfig dbConfig;

        public DatabaseHelper(DatabaseConfig _dbConfig)
        {
            dbConfig = _dbConfig;
            getConnect();
        }

        ~DatabaseHelper()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }

        public void getConnect()
        {
            _con = new OracleConnection();
            _con.ConnectionString = string.Format(connectionString,
                                                    dbConfig.host,
                                                    dbConfig.port,
                                                    dbConfig.servicename,
                                                    dbConfig.username,
                                                    dbConfig.password
                                                    );
            try
            {
                _con.Open();
            }
            catch (Exception e)
            {
                CloseDBConnection();
                throw e;
            }
        }

        public bool checkConnect()
        {
            if (_con != null)
            {
                return true;
            }
            return false;
        }

        public DataTable excuteSQL(string sql)
        {
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = _con;
            cmd.CommandText = sql;

            OracleDataReader dr = cmd.ExecuteReader();

            dt.Load(dr);
            return dt;
        }

        public DataTable excuteProcedure(string _proc, List<string> _params)
        {
            int idxPara = 0;
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = _con;
            cmd.CommandText = _proc;

            for (int i = 0; i < _params.Count; i++)
            {
                cmd.Parameters.Add(":p_" + i, _params[i]);
                idxPara = i;
            }

            //add return cursor
            cmd.Parameters.Add(":refcursor1", OracleDbType.RefCursor, ParameterDirection.Output);
            cmd.ExecuteNonQuery();
            OracleRefCursor curr = (OracleRefCursor)cmd.Parameters[idxPara + 1].Value;

            using (OracleDataReader dr = curr.GetDataReader())
            {
                dt.Load(dr);
            }

            return dt;
        }

        public string excuteCommand(string sql)
        {
            string message = "";
            if (checkConnect())
            {
                OracleCommand command = _con.CreateCommand();
                command.Connection = _con;

                command.CommandText = sql;

                try { command.ExecuteNonQuery(); message = "Command executed."; }
                catch (Exception e) { message = e.Message; }


            }
            else
            {
                message = "Database not Connected.";
            }
            return message;
        }

        public string excuteCommandBlob(string sql, List<byte[]> blobs)
        {
            string message = "";
            if (checkConnect())
            {
                OracleCommand command = _con.CreateCommand();
                command.Connection = _con;

                command.CommandText = sql;

                foreach (var blob in blobs)
                {
                    OracleParameter param = command.Parameters.Add("blobtodb", OracleDbType.Blob);
                    param.Direction = ParameterDirection.Input;
                    param.Value = blob;
                }

                try { command.ExecuteNonQuery(); message = "Command executed."; }
                catch (Exception e) { message = e.Message; }


            }
            else
            {
                message = "Database not Connected.";
            }
            return message;
        }

        public void CloseDBConnection()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }
    }
}